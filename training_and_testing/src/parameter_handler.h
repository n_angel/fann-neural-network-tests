/*
 * DifferentialEvolution.h
 *
 */


#ifndef PARAMETER_HANDLER_H
#define PARAMETER_HANDLER_H


//// Standard includes. /////////////////////////////////////////////////////
# include <cassert>
# include <random>
# include <iostream>
# include <vector>
# include <map>

//// Global Vars	 //////////////////////////////////////////////////////// 
//// Used namespaces. ///////////////////////////////////////////////////////
//// New includes. //////////////////////////////////////////////////////////

namespace PARAMETER
{
	std::map<std::string, std::string> unpack_parameters(int argc, char *argv[]);
	std::string get_value(std::map<std::string, std::string> params, std::string flag);
}  // Parameter Handler namespace

# endif