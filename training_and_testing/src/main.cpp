//// Standard includes. /////////////////////////////////////////////////////
# include <cstdio>
# include <iostream>
# include <vector>

 //// FANN includes. /////////////////////////////////////////////////////
# include "fann.h"
# include "floatfann.h"

 //// New includes. /////////////////////////////////////////////////////
# include "parameter_handler.h"
# include "fiostream.h"
# include "instann.h"



int main(int argc, char **argv)
{
	std::map<std::string, std::string> params = PARAMETER::unpack_parameters(argc, argv);

	struct fann *ann = train(params);

	/*
	std::string t_val;
	std::cout << "TEST SECTION" << std::endl;
	if ((t_val = PARAMETER::get_value(params, "-test")) == "true")
	{
		double test_error = test(ann, params);
		std::cout << "Total error: " << test_error << std::endl; 
	}
	*/

	fann_destroy(ann);

	std::cout << "************************************************************\n\n\n" << std::endl;
	return 0;
}