/*
 * parameter_handler.cpp
 *
 * Implementation of parameter_handler.h
 */

#include "instann.h"

//// Used namespaces. ///////////////////////////////////////////////////////
//// New includes. /////////////////////////////////////////////////////

//// Implemented functions. /////////////////////////////////////////////////

void set_weights(struct fann *ann)
{
	fann_randomize_weights(ann, -1, 1);
}

void show_activation_function_info(struct fann *ann)
{
	int num_layers = fann_get_num_layers(ann);
	std::cout << "num of layers: " << num_layers<< std::endl;

	unsigned int *layers;
	layers = (unsigned int *) malloc(num_layers * sizeof(unsigned int));
	FANN_API fann_get_layer_array(ann, layers);

	for (int it_l = 1; it_l < num_layers; ++it_l)
	{
		std::cout << "layer " << it_l << std::endl; // << " has " << layers[it_l] << " neurons" << std::endl; 

		for (int it_n = 0; it_n < layers[it_l]; ++it_n)
		{
			std::cout << "Activation function of neuron " << it_n << ": \t" << FANN_ACTIVATIONFUNC_NAMES[fann_get_activation_function(ann, it_l, it_n)] << std::endl;
		}
	}

	free(layers);
}

struct fann* train(std::map<std::string, std::string> params)
{
	//const float desired_error = (const float) 0.001;
	const float desired_error = (const float) std::stof(PARAMETER::get_value(params, "-error"));
	std::cout << "desired_error: " << desired_error << std::endl;

	const unsigned int max_epochs = std::stoi(PARAMETER::get_value(params, "-maxEpochs"));
	std::cout << "max_epochs: " << max_epochs << std::endl;

	const unsigned int epochs_between_reports = std::stoi(PARAMETER::get_value(params, "-epochsReports"));
	std::cout << "epochs_between_reports: " << epochs_between_reports << std::endl;

	// ** Layers info
	std::string layers_filename = PARAMETER::get_value(params, "-layers");
	std::cout << "layers: " << layers_filename << std::endl;
	std::vector<unsigned int> layers = load_layers(layers_filename);
	const unsigned int num_layers = (unsigned int) layers.size( );

	//k fold
	//const unsigned int kFold = std::stoi(PARAMETER::get_value(params, "-kFold"));
	//std::cout << "k fold: " << kFold << std::endl;

	struct fann *ann = fann_create_standard_array( num_layers, &layers[0] );
	fann_set_activation_function_hidden(ann, FANN_SIGMOID_SYMMETRIC);
	fann_set_activation_function_output(ann, FANN_SIGMOID_SYMMETRIC);

	//std::cout << "Activation function: " << FANN_ACTIVATIONFUNC_NAMES[fann_get_activation_function(ann, 1, 0)] << std::endl;
	//fann_set_activation_function_output(ann, FANN_SIGMOID);
	
	const float learning_rate = 1.e-3;
	fann_set_learning_rate( ann, learning_rate );

	show_activation_function_info(ann);

	std::cout << "\n\nParameters: " << std::endl;
	fann_print_parameters( ann );
	std::cout << "\n\nConnections: " << std::endl;
	fann_print_connections( ann );

	//exit(-1);


	/*	get training data	*/
	std::string dataset_filename = PARAMETER::get_value(params, "-trainingSet");
	std::cout << "training Set: " << dataset_filename << "\n" << std::endl;
	struct fann_train_data *data = fann_read_train_from_file(dataset_filename.c_str());

	//std::string testD_filename = PARAMETER::get_value(params, "-testDataset");
	//std::string testL_filename = PARAMETER::get_value(params, "-testLabels");
	//std::vector<INSTEST> data_test_full = load_test_datset(testD_filename, testL_filename);

	// Cross Validation
	//crossvalidation(kFold, max_epochs, epochs_between_reports, desired_error, ann, data, params);
	fann_train_on_data(ann, data, max_epochs, epochs_between_reports, desired_error);
	fann_type *calc_out;
	fann_type input[2] = {-1, 1};
	calc_out = fann_run(ann, input);
	std::cout << input[0] << "\t" << input[1] << "\t:\t" << calc_out[0] << std::endl;

	fann_destroy_train(data);

	return ann;
}

void crossvalidation(const unsigned int kFold, 
					const unsigned int max_epochs, 
					const unsigned int epochs_between_reports, 
					const float desired_error,
					struct fann *ann, struct fann_train_data *data,
					std::map<std::string, std::string> params)
{
	std::cout << std::fixed;
    std::cout << std::setprecision(10);

	struct timeval tim;

	int dataLength = (int)fann_length_train_data(data);
	// size of groups for Cross validation
	int sizeOfGroups = dataLength / kFold;
	std::cout << "\n\tsize of groups (kFold): " << sizeOfGroups << std::endl;

	fann_shuffle_train_data(data);

	double mean_time = 0.0;

	// Crossvalidation (k Folds)
	int pt_start = 0;
	std::vector< ERROR > errors(kFold);
	for (int it_k = 0; it_k < kFold; ++it_k)
	{
		/* set static weights (change with DDE values) */
		set_weights(ann);

		int bd_low_tr1 = 0;
		int bd_upp_tr1 = pt_start;

		int bd_low_tt1 = bd_upp_tr1;
		int bd_upp_tt1 = bd_low_tt1 + sizeOfGroups < dataLength? bd_low_tt1 + sizeOfGroups : dataLength;

		int bd_low_tr2 = bd_upp_tt1 < dataLength? bd_upp_tt1: dataLength;
		int bd_upp_tr2 = dataLength;

 		//std::cout << "train\t" << bd_low_tr1 << ", " << bd_upp_tr1 << " = " << bd_upp_tr1 - bd_low_tr1 << std::endl;
		struct 	fann_train_data* data_train1 = fann_subset_train_data(data, bd_low_tr1, bd_upp_tr1 - bd_low_tr1);

		//std::cout << "test\t" << bd_low_tt1 << ", " << bd_upp_tt1 << " = " << bd_upp_tt1 - bd_low_tt1 << std::endl;
		struct 	fann_train_data* data_test = fann_subset_train_data(data, bd_low_tt1, bd_upp_tt1 - bd_low_tt1);

		//std::cout << "train\t" << bd_low_tr2 << ", " << bd_upp_tr2 << " = " << bd_upp_tr2 - bd_low_tr2 << std::endl;
		struct 	fann_train_data* data_train2 = fann_subset_train_data(data, bd_low_tr2, bd_upp_tr2 - bd_low_tr2);
		
		struct 	fann_train_data* data_train = fann_merge_train_data(data_train1, data_train2);

		pt_start += sizeOfGroups;

		// Get start time
		gettimeofday(&tim, NULL);
    	double t1=tim.tv_sec+(tim.tv_usec/1000000.0);
    	double t1_tmp, t2;

    	double test_error;
    	std::string aux_flag;

		// training
		std::cout << "it \t train_error \t seconds_elapsed \t test_error"<< std::endl;
		for(int it_epochs=0; it_epochs<max_epochs; it_epochs++)
		{
			gettimeofday(&tim, NULL);
			t1_tmp = tim.tv_sec+(tim.tv_usec/1000000.0);

			std::cout << it_epochs << "\t";
			fann_train_epoch(ann, data_train);
			std::cout << test(data_test, ann) << "\t";

			// Get end time
			gettimeofday(&tim, NULL);
   			t2 = tim.tv_sec+(tim.tv_usec/1000000.0);
    		std::cout << t2-t1_tmp << "\t";

    		//std::cout << "\t\tflag: " << PARAMETER::get_value(params, "-test") << std::endl;
    		if ((aux_flag = PARAMETER::get_value(params, "-test")) == "true")
			{
    			test_error = test(ann, params);
    			std::cout << test_error;
    		}
		std::cout << std::endl;
		}

		//fann_train_on_data(ann, data_train, max_epochs, epochs_between_reports, desired_error);
		errors[it_k].mse = fann_test_data(ann, data_test);
		errors[it_k].classification = test(data_test, ann);

		// Get end time
		gettimeofday(&tim, NULL);
   		t2 = tim.tv_sec+(tim.tv_usec/1000000.0);
    	std::cout << "\n" << t2-t1 << " \t seconds elapsed\n\n\n" << std::endl;
    	//std::cout << (t2-t1) / kFold << " \t seconds elapsed per iteration" << std::endl;
    	mean_time += (t2-t1);

		fann_destroy_train(data_test);
		fann_destroy_train(data_train);
		fann_destroy_train(data_train1);
		fann_destroy_train(data_train2);
	}

	std::cout << "\n\n########################################################" << std::endl;
	std::cout << "Crossvalidation report" << std::endl;
	std::cout << "########################################################" << std::endl;
    print_error(errors);
	std::cout << "\n" << mean_time / kFold << " \t seconds elapsed per iteration (mean)" << std::endl;
	std::cout << "########################################################\n\n" << std::endl;
}

// To data vector and a trained network
double test(struct fann_train_data *data, struct fann *ann)
{
	fann_type *calc_out;
	int dataLength = (int)fann_length_train_data(data);
	int t_inputs = (int)fann_num_input_train_data(data);
	int t_outputs = (int)fann_num_output_train_data(data);

	int error = 0;
	for (int it_v = 0; it_v < dataLength; ++it_v)
	{
		calc_out = fann_run(ann, &data->input[it_v][0]);
		error += evaluate(calc_out, t_outputs, &data->output[it_v][0]);
	}

	return ((double)error * 100.0)/ (double) dataLength;
}

// Trained network and a file with test
double test(struct fann *ann, std::map<std::string, std::string> params)
{
	std::string testD_filename = PARAMETER::get_value(params, "-testDataset");
	std::string testL_filename = PARAMETER::get_value(params, "-testLabels");
	std::vector<INSTEST> data_test = load_test_datset(testD_filename, testL_filename);

	unsigned int t_inputs = fann_get_num_input(ann);
	unsigned int t_outputs = fann_get_num_output(ann);

	//FANN TESTING
	fann_type *calc_out;
	int error = 0;
	int it = 2;
	for (int it_t = 0; it_t < (int)data_test.size(); ++it_t)
	{
		calc_out = fann_run(ann, &data_test[it_t].inputs[0]);
		error += evaluate(calc_out, t_outputs, &data_test[it_t].outputs[0]);
		it ++;
	}

	return ((double)error * 100.0) / (double)data_test.size();
}

/**
 *	@brief	funcion que determina la clase para un patron
 *	@param 	fann_type *prediction salida de las neurona de la capa
 *	@return	1 No coincide con la clasificacion esperada
 *	@return	return 0:
 */
int evaluate(fann_type *prediction, int t_outputs, fann_type *expected)
{
	// Busca valor maximo de las salidas de cada neurona de la capa de salida
	fann_type MAX = prediction[0];
	prediction[0] = 1;
	int pos_max = 0;
	for (int i = 1; i < t_outputs; ++i)
	{
		if (prediction[i] > MAX)
		{
			MAX = prediction[i];
			prediction[pos_max] = 0;	// Pasa el status de la mejor clase anterior a cero
			prediction[i] = 1;	// Pone como clase asignada 
			pos_max = i;
		}
		else
		{
			prediction[i] = 0;	// El patron no pertenece a la clase i-esima
		}
	}
	
	return (int)prediction[pos_max] != expected[pos_max];
}

void print_error(std::vector< ERROR > errors)
{
	double mse = 0.0;
	double classification = 0.0;
	std::cout << "iteration\tMSE\tClassification" << std::endl;
	for (int it_e = 0; it_e < (int)errors.size(); ++it_e)
	{
		mse += errors[it_e].mse;
		classification += errors[it_e].classification;
		std::cout << it_e << " :\t" << errors[it_e].mse << " \t " << errors[it_e].classification << std::endl;
	}

	std::cout << "mean:\t" << mse / (double)errors.size() << " \t " <<  classification / (double)errors.size() << std::endl;
}

void print_results(unsigned int t_inputs, fann_type *inputs, unsigned int t_outputs, fann_type *ouputs)
{
	for (int it_i = 0; it_i < t_inputs; ++it_i)
	{
		std::cout << inputs[it_i] << " ";
	}
	std::cout << " -> ";

	for (int it_i = 0; it_i < t_outputs; ++it_i)
	{
		std::cout << ouputs[it_i] << " ";
	}
	std::cout << std::endl;
}
