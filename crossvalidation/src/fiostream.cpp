/*
 * parameter_handler.cpp
 *
 * Implementation of parameter_handler.h
 */

#include "fiostream.h"

//// Used namespaces. ///////////////////////////////////////////////////////
//// New includes. /////////////////////////////////////////////////////

//// Implemented functions. /////////////////////////////////////////////////


std::vector<unsigned int> load_layers(std::string filename)
{
	std::string line;
	std::vector<unsigned int> layers;

 	std::ifstream infile (filename);
 	if (infile.is_open())
 	{
 		getline (infile,line);
 		int lys = std::stoi(line);
		while ( lys )
 		{
 			getline (infile,line);
			layers.push_back((unsigned int) std::stoi(line));
			//std::cout << "Layer: " <<  layers[layers.size()-1] << std::endl;
			lys --;
		}
		infile.close();
	}
	else
	{
		perror("The file doesn't exist!");	
		exit(-1);	
	}

	return layers;
}


std::vector<INSTEST> load_test_datset(std::string filename_dataset, std::string filename_labels)
{
	std::vector<INSTEST> test_dataset;
	INSTEST aux;

	std::ifstream ifile_data(filename_dataset, std::ios::in);
	if (!ifile_data.is_open()) {
        std::cerr << "There was a problem opening the input file!\n";
        exit(1);//exit or do additional error checking
    }

    std::ifstream ifile_labels(filename_labels, std::ios::in);
	if (!ifile_labels.is_open()) {
        std::cerr << "There was a problem opening the input file!\n";
        exit(1);//exit or do additional error checking
    }

    double extra;
    unsigned int lbl_aux;
    int test_cases, label_cases;
    int vars, t_outs;
    ifile_data >> test_cases >> vars;
    ifile_labels >> label_cases >> t_outs;
    for (int it_i = 0; it_i < test_cases; ++it_i)
    {
     	std::vector<fann_type> vt;
     	for (int it_v = 0; it_v < vars; ++it_v)
     	{
        	ifile_data >> extra;
        	vt.push_back(extra);
     	}

     	std::vector<fann_type> outputs;
     	for (int it_o = 0; it_o < t_outs; ++it_o)
     	{
        	ifile_labels >> lbl_aux;
        	outputs.push_back(lbl_aux);
     	}

     	/*
     	for (int it_o = 0; it_o < t_outs; ++it_o)
     	{
        	std::cout << outputs[it_o] << " ";
     	}
     	std::cout << std::endl;
     	*/

     	aux.inputs = vt;
     	aux.outputs = outputs;
     	test_dataset.push_back(aux);
    }

    //ifile.close();
	return test_dataset;
	/*
	std::string line;
	std::vector<unsigned int> layers;

 	std::ifstream infile (filename);
 	if (infile.is_open())
 	{
 		getline (infile,line);
 		int lys = std::stoi(line);
		while ( lys )
 		{
 			getline (infile,line);
			layers.push_back((unsigned int) std::stoi(line));
			//std::cout << layers[layers.size()-1] << std::endl;
			lys --;
		}
		infile.close();
	}
	else
	{
		perror("The file doesn't exist!");	
		exit(-1);	
	}

	return layers;*/
}
