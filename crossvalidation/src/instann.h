/*
 * DifferentialEvolution.h
 *
 */

//instann
#ifndef INSTANN_H
#define INSTANN_H


//// Standard includes. /////////////////////////////////////////////////////
# include <cassert>
# include <random>
# include <fstream>
# include <iostream>
# include <vector>
# include <string>
# include <map>
# include <iomanip>

 //// New includes. //////////////////////////////////////////////////////////
 # include "parameter_handler.h"
 # include "fiostream.h"

  //// FANN includes. ////////////////////////////////////////////////////////
# include "fann.h"
//# include "floatfann.h"
//#include "parallel_fann.h"

//// Global Vars	 //////////////////////////////////////////////////////// 
//// Used namespaces. ///////////////////////////////////////////////////////

 typedef struct erors
 {
 	double mse;
 	double classification;
 }ERROR;


void set_weights(struct fann *ann);
void set_activation_function_info(struct fann *ann);
void print_ann_info(struct fann *ann);

//struct fann* train(std::map<std::string, std::string> params);
struct fann* build_ann(std::map<std::string, std::string> params);
void cross_validation(struct fann *ann, std::map<std::string, std::string> params);

void predict(int t_outputs, int *prediction, fann_type *calc_out);
double evaluate(struct fann *ann, struct fann_train_data* data_test);

/*
void crossvalidation(const unsigned int kFold, 
					const unsigned int max_epochs, 
					const unsigned int epochs_between_reports, 
					const float desired_error,
					struct fann *ann, struct fann_train_data *data,
					std::map<std::string, std::string> params);
//void test(struct fann *ann);
int evaluate(fann_type *prediction, int t_outputs, fann_type *expected);

void print_results(unsigned int t_inputs, fann_type *inputs, unsigned int t_outputs, fann_type *ouputs);
void print_error(std::vector< ERROR > errors);

double test(struct fann *ann, std::map<std::string, std::string> params);
double test(struct fann_train_data *data, struct fann *ann);
*/
# endif