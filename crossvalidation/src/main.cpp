//// Standard includes. /////////////////////////////////////////////////////
# include <cstdio>
# include <iostream>
# include <vector>

 //// FANN includes. /////////////////////////////////////////////////////
# include "fann.h"
# include "floatfann.h"

 //// New includes. /////////////////////////////////////////////////////
# include "parameter_handler.h"
# include "fiostream.h"
# include "instann.h"



int main(int argc, char **argv)
{
	std::map<std::string, std::string> params = PARAMETER::unpack_parameters(argc, argv);

	struct fann *ann = build_ann(params);
	print_ann_info(ann);
	
	cross_validation(ann, params);

	std::cout << "************************************************************\n\n\n" << std::endl;

	fann_destroy(ann);
	return 0;
}