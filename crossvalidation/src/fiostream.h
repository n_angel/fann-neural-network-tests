/*
 * DifferentialEvolution.h
 *
 */


#ifndef FIOSTREAM_H
#define FIOSTREAM_H


//// Standard includes. /////////////////////////////////////////////////////
# include <cassert>
# include <random>
# include <fstream>
# include <iostream>
# include <vector>
# include <string>
# include <map>

//// FANN includes. /////////////////////////////////////////////////////////
# include "fann.h"
# include "floatfann.h"

//// Global Vars	 //////////////////////////////////////////////////////// 
//// Used namespaces. ///////////////////////////////////////////////////////
//// New includes. //////////////////////////////////////////////////////////

 typedef struct test_instance
 {
 	std::vector<fann_type> inputs;
 	std::vector<fann_type> outputs;
 }INSTEST;

std::vector<unsigned int> load_layers(std::string filename);
std::vector<INSTEST> load_test_datset(std::string filename_dataset, std::string filename_labels);

# endif