from sklearn.neural_network import MLPClassifier

if __name__ == '__main__':
    nn = MLPClassifier(solver='lbfgs', alpha=1e-1, hidden_layer_sizes=(5, 2), random_state=0)